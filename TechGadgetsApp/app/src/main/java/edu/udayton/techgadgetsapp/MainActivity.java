package edu.udayton.techgadgetsapp;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //convert string array "technologies" to list

        List<String> Technologies =
                Arrays.asList(getResources().getStringArray(R.array.technologies));

        //inflate the gui list

        setListAdapter(new ArrayAdapter<String>(this,R.layout.activity_main,
                R.id.technology, Technologies));
    }// end of onCreate method

    protected void onListItemClick(ListView I, View v, int position, long id)
    {
        Intent intent=null;

        switch (position)
        {
            case 0:
                intent = new Intent(MainActivity.this, Picture.class);
                intent.putExtra(Picture.LBL_KEY,getResources().getString(R.string.txtAsus));
                intent.putExtra(Picture.ID_KEY,Integer.toString(R.drawable.asus));
                break;
            case 1:
                intent = new Intent(MainActivity.this, Picture.class);
                intent.putExtra(Picture.LBL_KEY,getResources().getString(R.string.txtPaytm));
                intent.putExtra(Picture.ID_KEY,Integer.toString(R.drawable.paytm1));
                break;
            case 2:
                intent = new Intent(MainActivity.this, Picture.class);
                intent.putExtra(Picture.LBL_KEY,getResources().getString(R.string.txtSony));
                intent.putExtra(Picture.ID_KEY,Integer.toString(R.drawable.xperia1));
                break;
            case 3:
                intent = new Intent(MainActivity.this, Picture.class);
                intent.putExtra(Picture.LBL_KEY,getResources().getString(R.string.txtCamera));
                intent.putExtra(Picture.ID_KEY,Integer.toString(R.drawable.camera1));
                break;
            case 4:
                intent = new Intent(MainActivity.this, Picture.class);
                intent.putExtra(Picture.LBL_KEY,getResources().getString(R.string.txtHotsolar));
                intent.putExtra(Picture.ID_KEY,Integer.toString(R.drawable.hotsolar2));
                break;
            default:
                Toast toast = Toast.makeText(MainActivity.this, "Invalid Choice", Toast.LENGTH_LONG);
                toast.show();
                break;
        }// end of switch

        //start activity
        startActivity(intent);


    }// end of onCreate method
}// end of MainActivity class
