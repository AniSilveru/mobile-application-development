package edu.udayton.techgadgetsapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class Picture extends AppCompatActivity {

    public static final String ID_KEY="RES_ID",LBL_KEY="LABEL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);

        Intent intent=getIntent();
        Bundle extras=intent.getExtras();
        if(extras !=null)
        {
            final String res_label=extras.getString(LBL_KEY);
            final TextView pictureTitle=(TextView)findViewById(R.id.titleTextView);
            pictureTitle.setText(res_label);

            String image_id=extras.getString(ID_KEY);
            int imageId=Integer.parseInt(image_id);

            final ImageView img=(ImageView)findViewById(R.id.pictureImageView);
            img.setImageResource(imageId);
            img.setContentDescription(res_label);

            Button infoBtn=(Button)findViewById(R.id.btnInfo);
            View.OnClickListener listner=new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    Intent intent_1=null;

                    if(res_label.equals(getString(R.string.txtAsus)))
                    {
                        intent_1 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://gadgets.ndtv.com/laptops/news/asus-vivobook-s-nanoedge-display-launched-specifications-price-availability-1716222"));
                    }

                    else if(res_label.equals(getString(R.string.txtPaytm)))
                    {
                        intent_1 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://gadgets.ndtv.com/mobiles/news/paytm-sale-iphone-7-iphone-7-plus-google-pixel-cashback-offers-1716249"));
                    }

                    else  if(res_label.equals(getString(R.string.txtSony)))
                    {
                        intent_1 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://gadgets.ndtv.com/mobiles/news/sony-xperia-x-xperia-x-compact-android-7-1-1-nougat-update-1716233"));
                    }

                    else  if(res_label.equals(getString(R.string.txtCamera)))
                    {
                        intent_1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.technologyreview.com/s/603496/10-breakthrough-technologies-2017-the-360-degree-selfie/"));
                    }

                    else  if(res_label.equals(getString(R.string.txtHotsolar)))
                    {
                        intent_1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.technologyreview.com/s/603497/10-breakthrough-technologies-2017-hot-solar-cells/"));
                    }

                    startActivity(intent_1);
                }
            };
            infoBtn.setOnClickListener(listner);
        }
    }
}
