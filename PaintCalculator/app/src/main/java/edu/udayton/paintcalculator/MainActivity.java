package edu.udayton.paintcalculator;

import android.icu.text.DecimalFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final double one_gallon=250;

    private int room_height;
    private int room_distance;
    private double totalArea;
    private double NoOfGallons;
    private String colorChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // button reference
            final Button btnGallons = (Button)findViewById(R.id.btnGallons);

        //validating btnGallons
        assert btnGallons !=null;

        View.OnClickListener gallonsListner = new View.OnClickListener()
        {

            final EditText hight = (EditText)findViewById(R.id.txtHight);
            final EditText distance =(EditText)findViewById(R.id.txtDistance);
            final Spinner group = (Spinner)findViewById(R.id.txtColor);
            final TextView result = (TextView)findViewById(R.id.txtResult);

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                // getting the input (height and distance)

                Editable Input_height = hight.getText();
                String StrHight = Input_height.toString();

                Editable Input_distance = distance.getText();
                String StrDistance = Input_distance.toString();

                // Exception Handling for no input

                try{

                    room_height = Integer.parseInt(StrHight);
                    room_distance = Integer.parseInt(StrDistance);

                    // Calculating the total area

                    totalArea = room_height * room_distance;

                    // Calculating the total number of gallons required

                    NoOfGallons = totalArea / one_gallon;

                    // chosen color from the spinner

                    colorChoice = group.getSelectedItem().toString();

                    //Setting the output format (rounding the decimal to 2 decimal places)

                    DecimalFormat totalGallons = new DecimalFormat("###,###.##");

                    //Displaying the output

                    String Output = " You require " +totalGallons.format(NoOfGallons) +
                            " gallons to paint the room with " +colorChoice;

                    result.setText(Output);
                }catch (Exception e){

                    Log.e(e.getMessage(),e.toString());
                }


            } // end of onClick method
        };

        // set the btnGallon Listner

        btnGallons.setOnClickListener(gallonsListner);

    } // end of onCreate method
}// end of MainActivity class
