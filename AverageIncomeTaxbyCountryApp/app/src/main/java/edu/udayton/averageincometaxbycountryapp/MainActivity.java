package edu.udayton.averageincometaxbycountryapp;

import android.app.Activity;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private double inputIncome , incomeTax;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button incomeTaxCal = (Button)findViewById(R.id.btnIncomeTax);

        final View.OnClickListener incomeTaxListner = new View.OnClickListener() {

            final EditText income = (EditText)findViewById(R.id.txtIncome);
            final RadioButton Country1 = (RadioButton)findViewById(R.id.radCountry1);
            final RadioButton Country2 = (RadioButton)findViewById(R.id.radCountry2);
            final RadioButton Country3 = (RadioButton)findViewById(R.id.radCountry3);
            final RadioButton Country4 = (RadioButton)findViewById(R.id.radCountry4);
            final TextView result = (TextView)findViewById(R.id.txtResult);

            final DecimalFormat formattor = new DecimalFormat("###,###,##.##");

            @Override
            public void onClick(View v) {

                String Input = income.getText().toString();

                String Output = "Invalid Data Entered";


                try{
                    inputIncome = Double.parseDouble(Input);

                    if(Country1.isChecked())
                    {
                        incomeTax = 0.25 * inputIncome;
                        Output = "Your past year's Income tax as a resident of China as a is " + formattor.format(incomeTax)+"Renminbi";
                }

                    else if(Country2.isChecked())
                        {
                            incomeTax = 0.32 * inputIncome;
                            Output = "Your past year's Income tax as a resident of Germany is " + formattor.format(incomeTax) +"Euros";
                        }

                    else if(Country3.isChecked())
                    {
                        incomeTax = 0.34 * inputIncome;
                        Output = "Your past year's Income tax as a resident of Sweden is " + formattor.format(incomeTax) +"Swedish Krona";
                    }

                    else
                    {
                        if(Country4.isChecked())
                        {
                            incomeTax = 0.18 * inputIncome;
                            Output = "Your past year's Income tax as a resident of USA is " + formattor.format(incomeTax) +"Dollars";
                        }
                    }

                }

                catch (Exception e){

                    Toast toast = Toast.makeText(MainActivity.this,e.toString(),Toast.LENGTH_LONG);
                    toast.show();
                }
                result.setText(Output);

            }
        };

        incomeTaxCal.setOnClickListener(incomeTaxListner);

    } //end of onCreate method
}//end of MainActivity class
