package edu.udayton.tourismguide;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class India extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Convert string array attraction to a list

        List<String> India =
                Arrays.asList(getResources().getStringArray(R.array.india));

        //inflate the GUI with the Attractions list

        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_india,
                R.id.spots, India));
    }

    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent intent= null;

        switch(position)
        {
            case 0:
                intent = new Intent(India.this, IndiaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(IndiaTourism.LBL_KEY,getResources().getString(R.string.txtAkshardham));
                intent.putExtra(IndiaTourism.ID_KEY, Integer.toString(R.drawable.akshardham));

                break;
            case 1:
                intent = new Intent(India.this, IndiaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(IndiaTourism.LBL_KEY,getResources().getString(R.string.txtChadar));
                intent.putExtra(IndiaTourism.ID_KEY, Integer.toString(R.drawable.chadar));


                break;

            case 2:
                intent = new Intent(India.this, IndiaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(IndiaTourism.LBL_KEY,getResources().getString(R.string.txtTaj));
                intent.putExtra(IndiaTourism.ID_KEY, Integer.toString(R.drawable.tajmahal));


                break;

            case 3:
                intent = new Intent(India.this, IndiaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(IndiaTourism.LBL_KEY,getResources().getString(R.string.txtGolden));
                intent.putExtra(IndiaTourism.ID_KEY, Integer.toString(R.drawable.goldentemple));


                break;
            case 4:
                intent = new Intent(India.this, IndiaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(IndiaTourism.LBL_KEY,getResources().getString(R.string.txtMunnar));
                intent.putExtra(IndiaTourism.ID_KEY, Integer.toString(R.drawable.munnar));


                break;

            default:
                Toast toast = Toast.makeText(India.this,
                        "Invalid Choice Mode", Toast.LENGTH_LONG);
                toast.show();

        }//end switch

        //start the activity via intent

        startActivity(intent);
    }

}
