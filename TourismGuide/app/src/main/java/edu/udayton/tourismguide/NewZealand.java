package edu.udayton.tourismguide;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class
        NewZealand extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Convert string array attraction to a list

        List<String> NewZealand =
                Arrays.asList(getResources().getStringArray(R.array.newzealand));

        //inflate the GUI with the Attractions list

        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_new_zealand,
                R.id.locations, NewZealand));
    }

    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent intent= null;

        switch(position)
        {
            case 0:
                intent = new Intent(NewZealand.this, NzTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(NzTourism.LBL_KEY,getResources().getString(R.string.txtAbel));
                intent.putExtra(NzTourism.ID_KEY, Integer.toString(R.drawable.abeltasmannationalpark));
                break;

            case 1:
                intent = new Intent(NewZealand.this, NzTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(NzTourism.LBL_KEY,getResources().getString(R.string.txtHuka));
                intent.putExtra(NzTourism.ID_KEY, Integer.toString(R.drawable.hukafalls));
                break;

            case 2:
                intent = new Intent(NewZealand.this, NzTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(NzTourism.LBL_KEY,getResources().getString(R.string.txtHobbiton));
                intent.putExtra(NzTourism.ID_KEY, Integer.toString(R.drawable.hobbitonmovieset));
                break;

            case 3:
                intent = new Intent(NewZealand.this, NzTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(NzTourism.LBL_KEY,getResources().getString(R.string.txtMuseumOfNz));
                intent.putExtra(NzTourism.ID_KEY, Integer.toString(R.drawable.museumofnz));
                break;

            case 4:
                intent = new Intent(NewZealand.this, NzTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(NzTourism.LBL_KEY,getResources().getString(R.string.txtSkyTower));
                intent.putExtra(NzTourism.ID_KEY, Integer.toString(R.drawable.skytower));
                break;

            default:
                Toast toast = Toast.makeText(NewZealand.this,
                        "Invalid Choice Mode", Toast.LENGTH_LONG);
                toast.show();

        }//end switch

        //start the activity via intent

        startActivity(intent);
    }
}
