package edu.udayton.tourismguide;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class France extends ListActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> France =
                Arrays.asList(getResources().getStringArray(R.array.france));

        //inflate the GUI with the Attractions list

        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_france,
                R.id.sites, France));
    }

    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent intent= null;

        switch(position)
        {
            case 0:
                intent = new Intent(France.this, FranceTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(FranceTourism.LBL_KEY,getResources().getString(R.string.txtEiffel));
                intent.putExtra(FranceTourism.ID_KEY, Integer.toString(R.drawable.eiffeltower));
                //intent.putExtra(Picture.BT_ID,getResources().getString(R.string.txtIpad));
                break;
            case 1:
                intent = new Intent(France.this, FranceTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(FranceTourism.LBL_KEY,getResources().getString(R.string.txtLouvre));
                intent.putExtra(FranceTourism.ID_KEY, Integer.toString(R.drawable.louvre));
                //intent.putExtra(Picture.BT_ID,getResources().getString(R.string.txtIphone));

                break;

            case 2:
                intent = new Intent(France.this, FranceTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(FranceTourism.LBL_KEY,getResources().getString(R.string.txtDisney));
                intent.putExtra(FranceTourism.ID_KEY, Integer.toString(R.drawable.disneyland));
                //intent.putExtra(Picture.BT_ID,getResources().getString(R.string.txtSamsung));

                break;

            case 3:
                intent = new Intent(France.this, FranceTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(FranceTourism.LBL_KEY,getResources().getString(R.string.txtParc));
                intent.putExtra(FranceTourism.ID_KEY, Integer.toString(R.drawable.parcasterix));
                //intent.putExtra(Picture.BT_ID,getResources().getString(R.string.txtCanon));

                break;
            case 4:
                intent = new Intent(France.this, FranceTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(FranceTourism.LBL_KEY,getResources().getString(R.string.txtCourchevel));
                intent.putExtra(FranceTourism.ID_KEY, Integer.toString(R.drawable.courchevel));
                //intent.putExtra(Picture.BT_ID,getResources().getString(R.string.txtBeats));

                break;

            default:
                Toast toast = Toast.makeText(France.this,
                        "Invalid Choice Mode", Toast.LENGTH_LONG);
                toast.show();

        }//end switch

        //start the activity via intent

        startActivity(intent);
    }

}
