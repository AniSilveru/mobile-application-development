package edu.udayton.tourismguide;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class NzTourism extends AppCompatActivity {

    public static final String ID_KEY = "RES_ID", LBL_KEY = "LABEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nz_tourism);


        //get the extras from the Intent

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null)
        {
            //get the String extra with key=LBL_KEY
            final String res_label = extras.getString(LBL_KEY);

            //display the labelString in the titleTextView
            final TextView locationTitle = (TextView)findViewById(R.id.locationTitle);
            locationTitle.setText(res_label);

            //get the String extra with key=ID_KEY
            String image_id = extras.getString(ID_KEY);

            //convert the resources ID from String to integer
            int imageId = Integer.parseInt(image_id);

            final ImageView nz = (ImageView)findViewById(R.id.locationImageView);
            nz.setImageResource(imageId);
            nz.setContentDescription(res_label);

            Button b2 = (Button)findViewById(R.id.btn2Select);

            View.OnClickListener b2Listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = null;
                    //String s = getIntent().getExtras().getString(BT_ID);
                    if(res_label.equals(getString(R.string.txtAbel)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.doc.govt.nz/parks-and-recreation/places-to-go/nelson-tasman/places/abel-tasman-national-park"));
                    }

                    else if(res_label.equals(getString(R.string.txtHuka)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.newzealand.com/int/feature/huka-falls"));
                    }
                    else if(res_label.equals(getString(R.string.txtHobbiton)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.hobbitontours.com"));
                    }
                    else if(res_label.equals(getString(R.string.txtMuseumOfNz)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://www.tepapa.govt.nz"));
                    }
                    else if(res_label.equals(getString(R.string.txtSkyTower)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://www.skycityauckland.co.nz/attractions/sky-tower"));
                    }


                    startActivity(i);
                }
            };

            b2.setOnClickListener(b2Listener);
        }

    }
}
