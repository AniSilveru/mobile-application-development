
package edu.udayton.tourismguide;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AmericaTourism extends AppCompatActivity {

    public static final String ID_KEY = "RES_ID", LBL_KEY = "LABEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_america_tourism);


        //get the extras from the Intent

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null)
        {
            //get the String extra with key=LBL_KEY
            final String res_label = extras.getString(LBL_KEY);

            //display the labelString in the titleTextView
            final TextView placeTitle = (TextView)findViewById(R.id.placeTitle);
            placeTitle.setText(res_label);

            //get the String extra with key=ID_KEY
            String image_id = extras.getString(ID_KEY);

            //convert the resources ID from String to integer
            int imageId = Integer.parseInt(image_id);

            final ImageView america = (ImageView)findViewById(R.id.placeImageView);
            america.setImageResource(imageId);
            america.setContentDescription(res_label);

            Button b1 = (Button)findViewById(R.id.btn1Select);

            View.OnClickListener b1Listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = null;

                    if(res_label.equals(getString(R.string.txtNiagara)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://www.niagarafallsusa.com"));
                    }

                    else if(res_label.equals(getString(R.string.txtSkydeck)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.theskydeck.com"));
                    }
                    else if(res_label.equals(getString(R.string.txtBryce)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://www.nps.gov/brca/index.htm-bryce national park"));
                    }
                    else if(res_label.equals(getString(R.string.txtHocking)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.thehockinghills.org"));
                    }
                    else if(res_label.equals(getString(R.string.txtTimesquare)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.timessquarenyc.org/index.aspx"));
                    }


                    startActivity(i);
                }
            };

            b1.setOnClickListener(b1Listener);
        }

    }
}
