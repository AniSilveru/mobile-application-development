package edu.udayton.tourismguide;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class IndiaTourism extends AppCompatActivity {

    public static final String ID_KEY = "RES_ID", LBL_KEY = "LABEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_india_tourism);

        //get the extras from the Intent

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null)
        {
            //get the String extra with key=LBL_KEY
            final String res_label = extras.getString(LBL_KEY);

            //display the labelString in the titleTextView
            final TextView spotTitle = (TextView)findViewById(R.id.spotTitle);
            spotTitle.setText(res_label);

            //get the String extra with key=ID_KEY
            String image_id = extras.getString(ID_KEY);

            //convert the resources ID from String to integer
            int imageId = Integer.parseInt(image_id);

            final ImageView picture = (ImageView)findViewById(R.id.spotImageView);
            picture.setImageResource(imageId);
            picture.setContentDescription(res_label);

            Button b = (Button)findViewById(R.id.btnSelect);

            View.OnClickListener bListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = null;
                    //String s = getIntent().getExtras().getString(BT_ID);
                    if(res_label.equals(getString(R.string.txtAkshardham)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://akshardham.com/"));
                    }

                    else if(res_label.equals(getString(R.string.txtChadar)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.chadartrek.com"));
                    }
                    else if(res_label.equals(getString(R.string.txtGolden)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.goldentempleamritsar.org"));
                    }
                    else if(res_label.equals(getString(R.string.txtTaj)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.tajmahal.gov.in"));
                    }
                    else if(res_label.equals(getString(R.string.txtMunnar)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.munnar.com"));
                    }


                    startActivity(i);
                }
            };

            b.setOnClickListener(bListener);
        }

    }
}
