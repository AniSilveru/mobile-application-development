package edu.udayton.tourismguide;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class Singapore extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<String> Singapore =
                Arrays.asList(getResources().getStringArray(R.array.singapore));

        //inflate the GUI with the Attractions list

        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_singapore,
                R.id.points, Singapore));
    }

    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent intent= null;

        switch(position)
        {
            case 0:
                intent = new Intent(Singapore.this, SingaporeTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(SingaporeTourism.LBL_KEY,getResources().getString(R.string.txtTiger));
                intent.putExtra(SingaporeTourism.ID_KEY, Integer.toString(R.drawable.tigerskytower));

                break;
            case 1:
                intent = new Intent(Singapore.this, SingaporeTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(SingaporeTourism.LBL_KEY,getResources().getString(R.string.txtMuseum));
                intent.putExtra(SingaporeTourism.ID_KEY, Integer.toString(R.drawable.museumsingapore));


                break;

            case 2:
                intent = new Intent(Singapore.this, SingaporeTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(SingaporeTourism.LBL_KEY,getResources().getString(R.string.txtRiverSafari));
                intent.putExtra(SingaporeTourism.ID_KEY, Integer.toString(R.drawable.riversafari));


                break;

            case 3:
                intent = new Intent(Singapore.this, SingaporeTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(SingaporeTourism.LBL_KEY,getResources().getString(R.string.txtSeaAqua));
                intent.putExtra(SingaporeTourism.ID_KEY, Integer.toString(R.drawable.seaaquarium));


                break;
            case 4:
                intent = new Intent(Singapore.this, SingaporeTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(SingaporeTourism.LBL_KEY,getResources().getString(R.string.txtUniversal));
                intent.putExtra(SingaporeTourism.ID_KEY, Integer.toString(R.drawable.universalstudios));


                break;

            default:
                Toast toast = Toast.makeText(Singapore.this,
                        "Invalid Choice Mode", Toast.LENGTH_LONG);
                toast.show();

        }//end switch

        //start the activity via intent

        startActivity(intent);
    }
}
