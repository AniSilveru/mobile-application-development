package edu.udayton.tourismguide;

import android.app.ListActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;


public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Convert string array attraction to a list

        List<String> Country =
                Arrays.asList(getResources().getStringArray(R.array.country));

        //inflate the GUI with the Attractions list

        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_main,
                R.id.travel, Country));

    }

    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent intent = null;

        switch(position)
        {
            case 0:
                intent = new Intent(MainActivity.this, India.class);
                break;
            case 1:
                intent = new Intent(MainActivity.this, USA.class);
                break;
            case 2:
                intent = new Intent(MainActivity.this, NewZealand.class);
                break;
            case 3:
                intent = new Intent(MainActivity.this,France.class);
                break;
            case 4:
                intent = new Intent(MainActivity.this,Singapore.class);
                break;
            default:
                Toast toast = Toast.makeText(MainActivity.this,
                        "Invalid Choice Mode", Toast.LENGTH_LONG);
                toast.show();
        }//end switch

        //start the activity via intent

        startActivity(intent);
    }//end onListItemClick method
}
