package edu.udayton.tourismguide;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class USA extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Convert string array attraction to a list

        List<String> Usa =
                Arrays.asList(getResources().getStringArray(R.array.usa));

        //inflate the GUI with the Attractions list

        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_us,
                R.id.places, Usa));
    }

    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent intent= null;

        switch(position)
        {
            case 0:
                intent = new Intent(USA.this, AmericaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(AmericaTourism.LBL_KEY,getResources().getString(R.string.txtNiagara));
                intent.putExtra(AmericaTourism.ID_KEY, Integer.toString(R.drawable.niagarafalls));

                break;
            case 1:
                intent = new Intent(USA.this, AmericaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(AmericaTourism.LBL_KEY,getResources().getString(R.string.txtSkydeck));
                intent.putExtra(AmericaTourism.ID_KEY, Integer.toString(R.drawable.skydeck));


                break;

            case 2:
                intent = new Intent(USA.this, AmericaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(AmericaTourism.LBL_KEY,getResources().getString(R.string.txtHocking));
                intent.putExtra(AmericaTourism.ID_KEY, Integer.toString(R.drawable.hockinghills));


                break;

            case 3:
                intent = new Intent(USA.this, AmericaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(AmericaTourism.LBL_KEY,getResources().getString(R.string.txtTimesquare));
                intent.putExtra(AmericaTourism.ID_KEY, Integer.toString(R.drawable.timessquare));


                break;
            case 4:
                intent = new Intent(USA.this, AmericaTourism.class);

                //add label text and resources is to the intent as String extras

                intent.putExtra(AmericaTourism.LBL_KEY,getResources().getString(R.string.txtBryce));
                intent.putExtra(AmericaTourism.ID_KEY, Integer.toString(R.drawable.brycecanyon));


                break;

            default:
                Toast toast = Toast.makeText(USA.this,
                        "Invalid Choice Mode", Toast.LENGTH_LONG);
                toast.show();

        }//end switch

        //start the activity via intent

        startActivity(intent);
    }

}
