package edu.udayton.tourismguide;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FranceTourism extends AppCompatActivity {

    public static final String ID_KEY = "RES_ID", LBL_KEY = "LABEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_france_tourism);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null)
        {
            //get the String extra with key=LBL_KEY
            final String res_label = extras.getString(LBL_KEY);

            //display the labelString in the titleTextView
            final TextView siteTitle = (TextView)findViewById(R.id.siteTitle);
            siteTitle.setText(res_label);

            //get the String extra with key=ID_KEY
            String image_id = extras.getString(ID_KEY);

            //convert the resources ID from String to integer
            int imageId = Integer.parseInt(image_id);

            final ImageView france = (ImageView)findViewById(R.id.siteImageView);
            france.setImageResource(imageId);
            france.setContentDescription(res_label);

            Button b3 = (Button)findViewById(R.id.btn3Select);

            View.OnClickListener b3Listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = null;

                    if(res_label.equals(getString(R.string.txtEiffel)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.toureiffel.paris"));
                    }

                    else if(res_label.equals(getString(R.string.txtDisney)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.disneylandparis.com"));
                    }
                    else if(res_label.equals(getString(R.string.txtCourchevel)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.courchevel.com"));
                    }
                    else if(res_label.equals(getString(R.string.txtLouvre)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.louvre.fr"));
                    }
                    else if(res_label.equals(getString(R.string.txtParc)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://www.parcasterix.fr/en"));
                    }


                    startActivity(i);
                }
            };

            b3.setOnClickListener(b3Listener);
        }

    }
}

