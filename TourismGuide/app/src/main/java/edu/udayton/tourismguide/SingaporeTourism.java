package edu.udayton.tourismguide;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SingaporeTourism extends AppCompatActivity {

    public static final String ID_KEY = "RES_ID", LBL_KEY = "LABEL";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singapore_tourism);


        //get the extras from the Intent

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null)
        {
            //get the String extra with key=LBL_KEY
            final String res_label = extras.getString(LBL_KEY);

            //display the labelString in the titleTextView
            final TextView pointTitle = (TextView)findViewById(R.id.pointTitle);
            pointTitle.setText(res_label);

            //get the String extra with key=ID_KEY
            String image_id = extras.getString(ID_KEY);

            //convert the resources ID from String to integer
            int imageId = Integer.parseInt(image_id);

            final ImageView singapore = (ImageView)findViewById(R.id.pointImageView);
            singapore.setImageResource(imageId);
            singapore.setContentDescription(res_label);

            Button b4 = (Button)findViewById(R.id.btn4Select);

            View.OnClickListener b4Listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = null;

                    if(res_label.equals(getString(R.string.txtMuseum)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://nationalmuseum.sg/"));
                    }

                    else if(res_label.equals(getString(R.string.txtTiger)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://store.sentosa.com.sg/main/attractions/tiger-sky-tower/14#!/"));
                    }
                    else if(res_label.equals(getString(R.string.txtRiverSafari)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.riversafari.com.sg/"));
                    }
                    else if(res_label.equals(getString(R.string.txtUniversal)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.rwsentosa.com/Homepage/Attractions/UniversalStudiosSingapore"));
                    }
                    else if(res_label.equals(getString(R.string.txtSeaAqua)))
                    {
                        i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://www.rwsentosa.com/Homepage/Attractions/SEAAquarium"));
                    }


                    startActivity(i);
                }
            };

            b4.setOnClickListener(b4Listener);
        }

    }

}

