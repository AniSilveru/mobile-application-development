package edu.udayton.concerttickets;

import android.icu.text.DecimalFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final double COST_PER_TICKET =79.99;

    private int numberOfTickets;
    private double totalCost;
    private String groupChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set the cost button reference

        final Button btnCost = (Button)findViewById(R.id.btnCost);

        // validate the btnCost reference

        assert btnCost !=null;

        View.OnClickListener costListner = new View.OnClickListener()
        {

            final EditText tickets = (EditText)findViewById(R.id.txtTickets);
            final Spinner group = (Spinner)findViewById(R.id.txtGroup);
            final TextView result = (TextView)findViewById(R.id.txtResult);
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // get number of tickets (user input)

                Editable Input = tickets.getText();
                String InputStr = Input.toString();

                // using exception handling in case of no input

                try {

                    numberOfTickets = Integer.parseInt(InputStr);

                    // calculating total cost of the tickets

                    totalCost = COST_PER_TICKET * numberOfTickets;

                    // get the chosen group from the spinner

                    groupChoice = group.getSelectedItem().toString();

                    // set up the currency formatter for ticket cost output

                    DecimalFormat currency = new DecimalFormat("$###,##.##");

                    //output the total ticket cost and the chosen group

                    String Output = "Cost for " +groupChoice + " is " + currency.format(totalCost);
                    result.setText(Output);
                } catch (Exception e)
                {
                    Log.e(e.getMessage(), e.toString());
                }


            } // end of onClick Method
        };

        //set the btnCOst Listener

        btnCost.setOnClickListener(costListner);
    }//end of onCreate method
}//end of MainActivity class
