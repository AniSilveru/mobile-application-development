package edu.udayton.cityguide;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //convert string array "attractions" to list

        List<String> Attractions =
                Arrays.asList(getResources().getStringArray(R.array.attraction));

        //inflate the gui with Attractions list

        setListAdapter(new ArrayAdapter<String>(this,R.layout.activity_main,
                R.id.travel, Attractions));
    }// end of onCreate method

    protected void onListItemClick(ListView I, View v, int position, long id)
    {
        Intent intent=null;

        switch (position)
        {
            case 0:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://artic.edu"));
                break;
            case  1:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://themagnificentmile.edu"));
                break;
            case 2:
                intent = new Intent(MainActivity.this, Willis.class);
                break;
            case 3:
                intent = new Intent(MainActivity.this, Pier.class);
                break;
            case 4:
                intent = new Intent(MainActivity.this, Water.class);
                break;
            default:
                Toast toast = Toast.makeText(MainActivity.this, "Invalid Choice Made", Toast.LENGTH_LONG);
                toast.show();
        }// switch

        //start activity
        startActivity(intent);

    }//onListItemClick method
}// end of MainActivity class
