package edu.udayton.healthyrecipes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button) findViewById(R.id.btnRecipe);

        // add event handler for button b

        View.OnClickListener bListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create an Intent for the recipe screen

                Intent i = new Intent(MainActivity.this, Recipe.class);

                // start new activity with Intent i

                startActivity(i);
            }
        };

        //set the Recipe button b's event handler

        b.setOnClickListener(bListener);
    }
}
