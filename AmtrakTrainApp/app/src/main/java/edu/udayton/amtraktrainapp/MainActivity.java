package edu.udayton.amtraktrainapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int boardingTimeHrs;
    private int boardingTimeMins;
    private int tripLength;

    public static final String DURATION_HOURS_KEY="key1",
            DURATION_MINUTES_KEY="key2",
            LENGTH_MINUTES="key3";

    public static final int DEFAULT_BOARDING_HOURS=0;
    public static final int DEFAULT_BOARDING_MINUTES=0;
    public static final int DEFAULT_LENGTH_MINUTES=0;

    private SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get btnArrival
        final Button btnArrival=(Button)findViewById(R.id.btnArrival);

        //create shared preferences object for this activity
            sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        //display persistent dat aif present
        showBoardingHours();
        showBoardingMinutes();
        showLengthMinutes();

        //create listener for btnArrival
        View.OnClickListener btnArrivalListener = new View.OnClickListener() {

            //get the GUI references for the text fields
            final EditText textBoardingTimeHours = (EditText)findViewById(R.id.txtHours);
            final EditText textBoardingTimeMinutes = (EditText)findViewById(R.id.txtMin);
            final EditText textLengthMinutes = (EditText)findViewById(R.id.txtLength);



            //helper method to validate the hours in boarding time, should be >0 and <=23
            private boolean validateDurationHours(int input){
                return (input>0 && input<=23);
            }//end of validateDurationHours

            //helper method to validate the minutes in boarding time, should be >0 and <=59
            private boolean validateDurationMinutes(int input){
                return (input>=0 && input<=59);
            }//end of validateDurationMinutes

            //helper method to validate the minutes in length of travel, should be >0 and <=1500
            private boolean validateLengthMinutes(int input){
                return(input>0 && input<=1500);
            }//end of validateLengthMinutes


            @Override
            public void onClick(View v) {

                //get the number of hours in duration convert it to integer with error checking
                String durationHours = textBoardingTimeHours.getText().toString();

                try{
                    boardingTimeHrs = Integer.parseInt(durationHours);

                    if(validateDurationHours(boardingTimeHrs)==false){
                        throw new Exception("Invalid Hours for Boarding time, Please enter value >0 and <=23");
                    }
                }
                catch(Exception e){
                    Toast toast = Toast.makeText(MainActivity.this,"Invalid Hours for Boarding time, Please enter value >0 and <=23",Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }

                //get the number of minutes in duration, convert it to integer with error checking
                String durationMinutes = textBoardingTimeMinutes.getText().toString();

                try{
                    boardingTimeMins = Integer.parseInt(durationMinutes);

                    if(validateDurationMinutes(boardingTimeMins)==false){
                        throw new Exception("Invalid Minutes for Boarding time, Please enter value >0 and <=59");
                    }
                }
                catch(Exception e){
                    Toast toast = Toast.makeText(MainActivity.this,"Invalid Minutes for Boarding time, Please enter value >0 and <=59",Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }


                //get the number of minutes in length of travel, convert it to integer with error checking
                String lengthMins = textLengthMinutes.getText().toString();

                try{
                    tripLength = Integer.parseInt(lengthMins);

                    if(validateLengthMinutes(tripLength)==false){
                        throw new Exception("Invalid Minutes for Length of travel, Please enter value >0 and <=1500");
                    }
                }
                catch(Exception e){
                    Toast toast = Toast.makeText(MainActivity.this,"Invalid Minutes for Length of travel, Please enter value >0 and <=1500",Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }

                //write persistent dat ato the shared preference object
                SharedPreferences.Editor editor= sharedPref.edit();
                editor.putInt(DURATION_HOURS_KEY,boardingTimeHrs);
                editor.putInt(DURATION_MINUTES_KEY,boardingTimeMins);
                editor.putInt(LENGTH_MINUTES,tripLength);
                editor.commit();

                //start the Arrival activity
                Intent intent = new Intent(MainActivity.this,Arrival.class);
                startActivity(intent);



            }//end of onClick event handler
        };

        btnArrival.setOnClickListener(btnArrivalListener);
    }//end onCreate
        private void showBoardingHours()
        {

            final EditText textBoardingTimeHours = (EditText)findViewById(R.id.txtHours);

            boardingTimeHrs = sharedPref.getInt(DURATION_HOURS_KEY,DEFAULT_BOARDING_HOURS);

            if(boardingTimeHrs > DEFAULT_BOARDING_HOURS){
                textBoardingTimeHours.setText(Integer.toString(boardingTimeHrs));
            }

        }

        private void showBoardingMinutes()
        {
            final EditText textBoardingTimeMinutes = (EditText)findViewById(R.id.txtMin);

            boardingTimeMins = sharedPref.getInt(DURATION_MINUTES_KEY,DEFAULT_BOARDING_MINUTES);

            if(boardingTimeMins > DEFAULT_BOARDING_MINUTES){
                textBoardingTimeMinutes.setText(Integer.toString(boardingTimeMins));
            }

        }

        //show the length of travel in minutes if present
        private void showLengthMinutes()
        {
            final EditText textLengthMinutes = (EditText)findViewById(R.id.txtLength);

            tripLength = sharedPref.getInt(LENGTH_MINUTES,DEFAULT_LENGTH_MINUTES);

            if(tripLength > DEFAULT_LENGTH_MINUTES){
                textLengthMinutes.setText(Integer.toString(tripLength));
            }

        }
}//end MainActivity
