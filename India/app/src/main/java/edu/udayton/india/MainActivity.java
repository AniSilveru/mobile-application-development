package edu.udayton.india;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         //Converting string array attraction to a list

        List<String> Attractions =
                Arrays.asList(getResources().getStringArray(R.array.attraction));

        //inflate the GUI with the Attractions list

        setListAdapter(new ArrayAdapter<String>(this,R.layout.activity_main,
        R.id.travel,Attractions));

    }// end onCreate

    protected void onListItemClick(ListView l, View v,int position,long id)
    {
        Intent intent=null;

        switch (position)
        {
            case 0:
                    intent = new Intent(MainActivity.this, History.class);
                break;
            case 1:
                intent = new Intent(MainActivity.this, Tourism.class);
                break;
            case 2:
                intent = new Intent(MainActivity.this, Food.class);
                break;
            case 3:
                intent = new Intent(MainActivity.this, Shopping.class);
                break;
            case 4:
                intent = new Intent(MainActivity.this, Sports.class);
                break;
            default:
                Toast toast = Toast.makeText(MainActivity.this,
                        "Invalid Choice Mode", Toast.LENGTH_LONG);
                toast.show();
        }
        startActivity(intent);
    }//end onListItemClick
}//end MainActivity
