package edu.udayton.electriccarfinancing;

import android.content.SharedPreferences;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

@RequiresApi(api = Build.VERSION_CODES.N)
public class Payment extends AppCompatActivity {


    private SharedPreferences sharedPref;

    private final DecimalFormat currency = new DecimalFormat("$###,###.00");

    private  static final  int MONTHS_PER_YEAR = 12;

    private  static  final  String MONTHLY_PAYMENT_PREFIX = "Monthly Payment: ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
    }
}
