package edu.udayton.medicalcalculator;

import android.app.Activity;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private final double CONVERSION_RATE = 2.2;
    private final int LB_LIMIT = 500, KILO_LIMIT = 225;

    private double weightEntered, convertedWeight;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //convert button refererence

        final Button convert =(Button)findViewById(R.id.btnConvert);

        //convert button listner

        View.OnClickListener convertListner = new View.OnClickListener() {

            final EditText weight = (EditText)findViewById(R.id.txtWeight);
            final RadioButton lbsToKilos = (RadioButton)findViewById(R.id.radLbToKilo);
            final RadioButton kiloToLbs = (RadioButton)findViewById(R.id.radKiloToLb);
            final TextView result = (TextView)findViewById(R.id.txtResult);

            final DecimalFormat formatter = new DecimalFormat("#.#");
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // get user input (weight) as string

                String Input = weight.getText().toString();

                //set up output string default to error msg

                String Output = "Invalid Data Entered";

                // convert only with valid data
                try{
                    weightEntered = Double.parseDouble(Input);

                    // check which conversion is being done
                    if(lbsToKilos.isChecked())
                    {
                        // only convert if not greater than LB_LIMIT
                        if(weightEntered<LB_LIMIT)
                        {
                            convertedWeight = weightEntered / CONVERSION_RATE;

                            Output = formatter.format(convertedWeight) + "kilograms";
                        }
                    }// end of pounds to kilos

                    else // kilos to pounds
                    {
                        if(weightEntered<KILO_LIMIT) {

                            convertedWeight = weightEntered*CONVERSION_RATE;

                            Output = formatter.format(convertedWeight) + "pounds";

                        }
                    } // end of pounds to kilos
                }
                catch (Exception e){

                    Toast toast = Toast.makeText(MainActivity.this, e.toString(),Toast.LENGTH_LONG);
                    toast.show();
                }
                //display output to result feild
                result.setText(Output);

            }// end of onClick method
        };
        //set convert button listner
        convert.setOnClickListener(convertListner);

    }// end of onCreate method
}// end of MainActivity class
