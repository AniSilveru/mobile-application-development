package edu.udayton.thelatestmusicscene;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1 = (Button) findViewById(R.id.btnNews11);
        Button b2 = (Button) findViewById(R.id.btnNews12);

        //creating an event for button b1

        View.OnClickListener b1Listner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // creating Intent for first screen

                Intent i1 = new Intent(MainActivity.this, News1.class);

                //starting new Activity with Intent i1

                startActivity(i1);

            }
        };

        //creating an event for b2

        View.OnClickListener b2Listner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // creating Intent for the first screen

                Intent i2 = new Intent(MainActivity.this, News2.class);

                //starting new activity with Intent i2

                startActivity(i2);
            }
        };

        //Setting the News1 button b1's event handler

        b1.setOnClickListener(b1Listner);

        //Setting the News2 button b2's event handler

        b2.setOnClickListener(b2Listner);
    }
}
