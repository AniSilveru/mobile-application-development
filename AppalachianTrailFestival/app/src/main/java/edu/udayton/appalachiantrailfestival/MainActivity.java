package edu.udayton.appalachiantrailfestival;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private TextView txtTickets; //output TextView reference

    private static final Calendar cal = Calendar.getInstance();
    private static final DateFormat fmtDate = DateFormat.getDateInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get gui control reference
        txtTickets = (TextView) findViewById(R.id.txtTickets);
        Button btnDate = (Button) findViewById(R.id.btnDate);

        //listener for btnDate
        btnDate.setOnClickListener(btnListener);
    }// end onCreate


    // set up the listener for when the user chooses the date
    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener()
    {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            //set the selected date and time in the Calender
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, monthOfYear);
            cal.set(Calendar.DAY_OF_MONTH,dayOfMonth);

            //get the selected date from calender and show it to user
            Date reservedDate = cal.getTime();
            txtTickets.setText("Your Tickets are available on " + fmtDate.format(reservedDate));

        }//end onDateSet
    };

    // set up the listener for the btnDate Button
    Button.OnClickListener btnListener = new Button.OnClickListener()
    {

        @Override
        public void onClick(View v) {
            int year = cal.get(Calendar.YEAR);
            int monthOfYear = cal.get(Calendar.MONTH);
            int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

            //create and show the DatePickerDialog with currently selected date
            DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                    dateSetListener, year, monthOfYear, dayOfMonth);
            datePickerDialog.show();

        }//onClick
    };
}//end MainActivity
