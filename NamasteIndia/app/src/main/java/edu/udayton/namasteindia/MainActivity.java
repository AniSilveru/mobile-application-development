package edu.udayton.namasteindia;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> Attractions =
                Arrays.asList(getResources().getStringArray(R.array.attraction));

        setListAdapter(new ArrayAdapter<String>(this,R.layout.activity_main,
                R.id.travel, Attractions));
    }

    protected void onListItemClick(ListView I, View v, int position, long id)
    {
        Intent intent=null;

        switch (position)
        {
            case 0:
                intent = new Intent(MainActivity.this, History.class);
                break;
            //case 1:
              //  intent = new Intent(MainActivity.this, History.class);
                //break;

            default:
                Toast toast = Toast.makeText(MainActivity.this, "Invalid Choice Made", Toast.LENGTH_LONG);
                toast.show();
        }// switch

        //start activity
        startActivity(intent);

    }//onListItemClick
}

